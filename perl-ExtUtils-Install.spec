%define mod_name ExtUtils-Install
Name:           perl-%{mod_name}
Version:        2.22
Release:        2
Summary:        Install Perl files from here to there
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/B/BI/BINGOS/%{mod_name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils make
BuildRequires:  perl-interpreter perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(lib) perl(strict) sed
BuildRequires:  perl(AutoSplit) perl(Carp) perl(Config)
BuildRequires:  perl(Cwd) perl(Exporter) perl(File::Basename)
BuildRequires:  perl(File::Compare) perl(File::Copy) perl(File::Find)
BuildRequires:  perl(File::Path) perl(File::Spec) perl(vars)
BuildRequires:  perl(diagnostics) perl(ExtUtils::MM)
BuildRequires:  perl(File::Temp) perl(Test::More)
Requires:       perl(AutoSplit) perl(Data::Dumper) perl(File::Compare)
Recommends:     perl(POSIX)

%{?perl_default_filter}

%description
Perl modules installing and uninstalling

%package            help
Summary:            Documents for %{name}
Buildarch:          noarch

%description        help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{mod_name}-%{version} -p1
rm -rf t/lib/Test
sed -i -e '/^t\/lib\/Test\//d' MANIFEST

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test

%files
%doc Changes README
%{perl_vendorlib}/*

%files help
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2.22-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jul 19 2023 leeffo <liweiganga@uniontech.com> - 2.22-1
- upgrade to version 2.22

* Tue Oct 25 2022 zhoushuiqing <zhoushuiqing2@huawei.com> - 2.20-2
- define mod_name to opitomize the specfile

* Fri Jan 29 2021 yuanxin <yuanxin24@huawei.com> - 2.20-1
- upgrade version to 2.20

* Thu Jul 23 2020 xinghe <xinghe1@huawei.com> - 2.16-1
- update version to 2.16

* Sat Sep 14 2019 zhangsaisai <zhangsaisai@huawei.com> - 2.14-419
- Package Init
